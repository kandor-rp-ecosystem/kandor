package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/handlers"
)

func RegisterAuthRoutes(e *echo.Echo) {
	e.POST("/register", handlers.RegisterAccount)
	e.POST("/login", handlers.LoginAccount)
}
