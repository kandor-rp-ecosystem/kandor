package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/handlers"
)

func RegisterPermissionRoutes(e *echo.Echo) {
	e.GET("/permission/:idPermission", handlers.GetPermissionById)
	e.GET("/permission/name/:idPermission", handlers.GetPermissionByName)
	e.POST("/permission", handlers.CreatePermission)
	e.GET("/permission", handlers.GetAllPermissionsWithPagination)
	//e.PUT("/group/:idGroup", handlers.UpdateGroup)
	//e.DELETE("/group/:idGroup", handlers.DeleteGroup)
	// Relations
	//e.POST("/group/:idGroup/permission/:idPermission", groupHandler.AddPermissionToGroup)
	//e.DELETE("/group/:idGroup/permission/:idPermission", groupHandler.RemovePermissionFromGroup)
}
