package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/handlers"
)

func RegisterAccountRoutes(e *echo.Echo) {
	e.GET("/account/:uuid", handlers.GetAccount)
	e.GET("/account", handlers.GetAccounts)
	e.PATCH("/account/:uuid", handlers.UpdateAccount)
	e.DELETE("/account/:uuid", handlers.SoftDelete)
	e.POST("/account/:uuid/group/:idGroup", handlers.AddGroup)
	e.DELETE("/account/:uuid/group/:idGroup", handlers.RemoveGroup)
}
