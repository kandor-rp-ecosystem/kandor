package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/handlers"
)

func RegisterGroupRoutes(e *echo.Echo) {
	e.POST("/group", handlers.CreateGroup)
	e.GET("/group/:idGroup", handlers.GetGroupById)
	e.GET("/group/name/:name", handlers.GetGroupByName)
	e.GET("/group", handlers.GetAllGroupsWithPagination)
	e.PUT("/group/:idGroup", handlers.UpdateGroup)
	e.DELETE("/group/:idGroup", handlers.DeleteGroup)
	// Relations
	//e.POST("/group/:idGroup/permission/:idPermission", groupHandler.AddPermissionToGroup)
	//e.DELETE("/group/:idGroup/permission/:idPermission", groupHandler.RemovePermissionFromGroup)
}
