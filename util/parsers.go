package utils

import "strconv"

func ParseUint(numberText string) (*uint, error) {
	val, err := strconv.ParseUint(numberText, 10, 64)
	if err != nil {
		return nil, err
	}
	uintVal := uint(val)
	return &uintVal, err
}
