package db

import (
	"database/sql"
	"os"

	"github.com/joho/godotenv"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"github.com/uptrace/bun/extra/bundebug"
)

var Bun *bun.DB

func CreateDatabase() (*sql.DB, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, err
	}
	user := os.Getenv("POSTGRESQL_USER")
	password := os.Getenv("POSTGRESQL_PASSWORD")
	host := os.Getenv("POSTGRESQL_HOST")
	port := os.Getenv("POSTGRESQL_PORT")
	dbName := os.Getenv("POSTGRESQL_DATABASE")
	connectionString := "postgres://" + user + ":" + password + "@" + host + ":" + port + "/" + dbName + "?sslmode=disable"
	db := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(connectionString)))
	Bun = bun.NewDB(db, pgdialect.New())
	Bun.AddQueryHook(bundebug.NewQueryHook(bundebug.WithVerbose(true)))
	if err := Bun.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
