DB_USER := root
DB_PASS := root
DB_NAME := kandor
DB_HOST := localhost
DB_PORT := 5432

DATABASE_URL := "postgresql://$(DB_USER):$(DB_PASS)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)"

# Install golang-migrate
install-migrate:
	curl -L https://github.com/golang-migrate/migrate/releases/download/4.16.2/migrate.linux-amd64.tar.gz | tar xvz

migrate-up:
	migrate -path ./db/migrations -database $(DATABASE_URL)?sslmode=disable up

migrate-down:
	migrate -path ./db/migrations -database $(DATABASE_URL)?sslmode=disable down 1

create-migration:
	migrate create -ext sql -dir ./db/migrations -seq $(name)

.PHONY: install-migrate migrate-up migrate-down create-migration

# Example: make create-migration name=migration_name
