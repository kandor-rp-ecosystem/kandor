package main

import (
	"context"

	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/db"
)

func main() {

	ctx := context.TODO()
	_, err := db.CreateDatabase()
	if err != nil {
		panic(err)
	}
	db.Bun.NewCreateTable().Model(&models.Account{}).IfNotExists().Exec(ctx)
	db.Bun.NewCreateTable().Model(&models.AccountGroup{}).IfNotExists().Exec(ctx)
	db.Bun.NewCreateTable().Model(&models.Group{}).IfNotExists().Exec(ctx)
}
