-- Create the account table
CREATE TABLE IF NOT EXISTS "account" (
  "uuid" UUID PRIMARY KEY,
  "balance" DECIMAL(10,2) NOT NULL DEFAULT 0,
  "mined_blocks" BIGINT NOT NULL DEFAULT 0,
  "password" VARCHAR(80) NOT NULL,
  "email" VARCHAR(255),
  "email_verified" BOOLEAN NOT NULL DEFAULT FALSE,
  "last_login" TIMESTAMP WITH TIME ZONE,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "deleted_at" TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS "group" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(255) UNIQUE NOT NULL,
  "description" VARCHAR(255),
  "prefix" VARCHAR(5) UNIQUE,
  "suffix" VARCHAR(5) UNIQUE,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "deleted_at" TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS "group_inheritance" (
  "parent_id" SERIAL NOT NULL,
  "child_id" SERIAL NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("parent_id", "child_id"),
  FOREIGN KEY ("parent_id") REFERENCES "group" ("id") ON DELETE CASCADE,
  FOREIGN KEY ("child_id") REFERENCES "group" ("id") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "account_group" (
  "uuid" UUID NOT NULL,
  "id_group" SERIAL NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("uuid", "id_group"),
  FOREIGN KEY ("uuid") REFERENCES "account" ("uuid"),
  FOREIGN KEY ("id_group") REFERENCES "group" ("id") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "permission"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(255) UNIQUE NOT NULL,
  "description" VARCHAR(255),
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "deleted_at" TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS "account_permission" (
  "uuid" UUID NOT NULL,
  "id_permission" SERIAL NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "is_negated" BOOLEAN NOT NULL DEFAULT FALSE,
  "world" VARCHAR(255) NOT NULL DEFAULT '*',
  "expires_at" TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  PRIMARY KEY ("uuid", "id_permission"),
  FOREIGN KEY ("uuid") REFERENCES "account" ("uuid"),
  FOREIGN KEY ("id_permission") REFERENCES "permission" ("id") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "group_permission" (
  "id_group" SERIAL NOT NULL,
  "id_permission" SERIAL NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  "is_negated" BOOLEAN NOT NULL DEFAULT FALSE,
  "world" VARCHAR(255) NOT NULL DEFAULT '*',
  "expires_at" TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  PRIMARY KEY ("id_group", "id_permission"),
  FOREIGN KEY ("id_group") REFERENCES "group" ("id"),
  FOREIGN KEY ("id_permission") REFERENCES "permission" ("id") ON DELETE CASCADE
);
