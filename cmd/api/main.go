package main

import (
	"log"

	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/db"
	"gitlab.com/kandor-rp-ecosystem/kandor/routes"
)

func main() {
	// Initialize database
	_, err := db.CreateDatabase()
	if err != nil {
		panic(err)
	}

	e := echo.New()

	// Routes
	routes.RegisterAuthRoutes(e)
	routes.RegisterAccountRoutes(e)
	routes.RegisterGroupRoutes(e)
	routes.RegisterPermissionRoutes(e)

	if err := e.Start(":8080"); err != nil {
		log.Fatal(err)
	}
}
