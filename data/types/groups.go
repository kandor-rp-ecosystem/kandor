package types

import validation "github.com/go-ozzo/ozzo-validation"

type Group struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Prefix      string `json:"prefix"`
	Suffix      string `json:"suffix"`
}

type CreateGroup struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Prefix      string `json:"prefix"`
	Suffix      string `json:"suffix"`
}

func (a *CreateGroup) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.Name, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Description, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Prefix, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Suffix, validation.Required, validation.Length(6, 100)),
	)
}

type GroupId struct {
	Id uint `json:"id"`
}

type UpdateGroup struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Prefix      string `json:"prefix"`
	Suffix      string `json:"suffix"`
}

func (a *UpdateGroup) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.Name, validation.Length(6, 100)),
		//validation.Field(&a.Description, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Prefix, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Suffix, validation.Required, validation.Length(6, 100)),
	)
}
