package types

type PaginationMeta struct {
	CurrentPage  int
	TotalPages   int
	TotalItems   int
	ItemsPerPage int
}
