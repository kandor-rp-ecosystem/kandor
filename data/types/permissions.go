package types

import validation "github.com/go-ozzo/ozzo-validation"

type Permission struct {
	Id          uint64 `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type CreatePermission struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (p *CreatePermission) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.Name, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Description, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Prefix, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Suffix, validation.Required, validation.Length(6, 100)),
	)
}

type PermissionId struct {
	Id uint64 `json:"id"`
}

type UpdatePermission struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (p *UpdatePermission) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.Name, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Description, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Prefix, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Suffix, validation.Required, validation.Length(6, 100)),
	)
}
