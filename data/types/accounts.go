package types

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

type RegisterAccount struct {
	UUID     string `json:"uuid"`
	Password string `json:"password"`
}

type AccountUUID struct {
	UUID string `json:"uuid"`
}

func (a *RegisterAccount) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.UUID, validation.Required, validation.Length(6, 100)),
		validation.Field(&a.Password, validation.Required, validation.Length(6, 100)),
	)
}

type LoginAccountWithMinecraft struct {
	UUID     string `json:"uuid"`
	Password string `json:"password"`
}

func (a *LoginAccountWithMinecraft) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.UUID, validation.Required, validation.Length(6, 100)),
		validation.Field(&a.Password, validation.Required, validation.Length(6, 100)),
	)
}

type LoginResponse struct {
	Token string `json:"token"`
}

type UpdateAccount struct {
	Password      string  `json:"password"`
	Balance       float64 `json:"balance"`
	MinedBlocks   uint64  `json:"mined_blocks"`
	Email         string  `json:"email"`
	EmailVerified bool    `json:"email_verified"`
}

func (a *UpdateAccount) Validate() error {
	return validation.ValidateStruct(a,
		validation.Field(&a.Password, validation.Required, validation.Length(6, 100)),
		//validation.Field(&a.Balance, validation.NilOrNotEmpty),
		//validation.Field(&a.MinedBlocks, validation.NilOrNotEmpty),
		validation.Field(&a.Email, validation.Required, validation.Length(6, 100)),
		validation.Field(&a.EmailVerified, validation.Required),
	)
}

type Account struct {
	UUID          string    `json:"uuid"`
	Password      string    `json:"password"`
	Balance       float64   `json:"balance"`
	MinedBlocks   uint64    `json:"mined_blocks"`
	Email         string    `json:"email"`
	EmailVerified bool      `json:"email_verified"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	DeletedAt     time.Time `json:"deleted_at"`
}
