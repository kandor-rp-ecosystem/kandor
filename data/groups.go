package data

import (
	"context"
	"log/slog"

	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/db"
	"gitlab.com/kandor-rp-ecosystem/kandor/types"
)

func GetGroupById(ctx context.Context, idGroup uint) (*models.Group, error) {
	var groupResult models.Group
	err := db.Bun.NewSelect().Model(&groupResult).Where("id = ?", idGroup).Scan(ctx)
	if err != nil {
		slog.Error(types.GroupRepository, "GetGroupById", err)
		return nil, err
	}
	return &groupResult, nil
}

func GetGroupByName(ctx context.Context, name string) (*models.Group, error) {
	var groupResult models.Group
	err := db.Bun.NewSelect().Model(&groupResult).Where("name = ?", name).Scan(ctx)
	if err != nil {
		slog.Error(types.GroupRepository, "GetGroupByName", err)
		return nil, err
	}
	return &groupResult, nil
}

// TODO: esto está mal, hay que devolver metadata de la paginación
func GetAllGroupsWithPagination(ctx context.Context, page int, size int) ([]models.Group, error) {
	var groups []models.Group
	err := db.Bun.NewSelect().Model(&groups).Limit(size).Offset((page - 1) * size).Scan(ctx)
	if err != nil {
		slog.Error(types.GroupRepository, "GetAllGroupsWithPagination", err)
		return nil, err
	}
	return groups, nil
}

func CreateGroup(ctx context.Context, group models.Group) (*models.Group, error) {
	var groupResult models.Group
	_, err := db.Bun.NewInsert().Model(&group).Returning("id").Exec(ctx, &groupResult.Id)
	if err != nil {
		slog.Error(types.GroupRepository, "CreateGroup", err)
		return nil, err
	}
	return &models.Group{Id: groupResult.Id}, nil
}

func UpdateGroup(ctx context.Context, id uint, group models.Group) (*models.Group, error) {
	var groupResult models.Group
	_, err := db.Bun.NewUpdate().Model(&group).OmitZero().Returning("id").Where("id = ?", id).Exec(ctx, &groupResult.Id)
	if err != nil {
		slog.Error(types.GroupRepository, "UpdateGroup", err)
		return nil, err
	}
	return &groupResult, nil
}

func DeleteGroup(ctx context.Context, idGroup uint) (*models.Group, error) {
	var groupResult models.Group
	_, err := db.Bun.NewDelete().Model(&groupResult).Returning("id").Where("id = ?", idGroup).Exec(ctx, &groupResult.Id)
	if err != nil {
		slog.Error(types.GroupRepository, "DeleteGroup", err)
		return nil, err
	}
	return &models.Group{Id: groupResult.Id}, nil
}

func AddPermissionToGroup(ctx context.Context, idGroup uint, idPermission uint) (*models.Group, error) {
	var groupResult models.Group
	return &models.Group{Id: groupResult.Id}, nil
}

func RemovePermissionFromGroup(ctx context.Context, idGroup uint, idPermission uint) (*models.Group, error) {
	var groupResult models.Group
	return &models.Group{Id: groupResult.Id}, nil
}
