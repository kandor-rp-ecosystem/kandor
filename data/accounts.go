package data

import (
	"context"
	"log/slog"

	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/db"
	"gitlab.com/kandor-rp-ecosystem/kandor/types"
)

//TODO: manejar errores

func GetAccount(ctx context.Context, UUID string) (*models.Account, error) {
	var accountResult models.Account
	err := db.Bun.NewSelect().Model(&accountResult).Where("uuid = ?", UUID).Scan(ctx)
	if err != nil {
		slog.Error(types.AccountRepository, "GetAccount", err)
		return nil, err
	}
	return &accountResult, nil
}

func GetAccounts(ctx context.Context) ([]models.Account, error) {
	var accounts []models.Account
	err := db.Bun.NewSelect().Model(&accounts).Scan(ctx)
	if err != nil {
		slog.Error(types.AccountRepository, "GetAccounts", err)
		return nil, err
	}
	return accounts, nil
}

func RegisterAccount(ctx context.Context, account models.Account) (*models.Account, error) {
	var accountResult models.Account
	_, err := db.Bun.NewInsert().Column("uuid", "password").Model(&account).Returning("uuid").Exec(ctx, &accountResult.UUID)
	if err != nil {
		slog.Error(types.AccountRepository, "RegisterAccount", err)
		return nil, err
	}
	return &accountResult, nil
}

func LoginAccountWithMinecraft(ctx context.Context, account models.Account) (*models.Account, error) {
	var accountResult models.Account
	err := db.Bun.NewSelect().Model(&accountResult).Where("uuid = ?", account.UUID).Scan(ctx)
	if err != nil {
		slog.Error(types.AccountRepository, "LoginAccountWithMinecraft", err)
		return nil, err
	}
	return &accountResult, nil
}

func UpdateLastLogin(ctx context.Context, UUID string) (*models.Account, error) {
	var accountResult models.Account
	_, err := db.Bun.NewUpdate().Model(&accountResult).Set("last_login = now()").Where("uuid = ?", UUID).Exec(ctx)
	if err != nil {
		slog.Error(types.AccountRepository, "UpdateLastLogin", err)
		return nil, err
	}
	return &accountResult, nil
}

func UpdateAccount(ctx context.Context, uuid string, account models.Account) (*models.Account, error) {
	var accountResult models.Account
	_, err := db.Bun.NewUpdate().Model(&account).OmitZero().Returning("uuid").Where("uuid = ?", uuid).Exec(ctx, &accountResult.UUID)
	if err != nil {
		slog.Error(types.AccountRepository, "UpdateAccount", "Error updating account:", err)
		return nil, err
	}
	return &accountResult, nil
}

func SoftDelete(ctx context.Context, UUID string) (*models.Account, error) {
	var accountResult models.Account
	_, err := db.Bun.NewUpdate().Model(&accountResult).Returning("uuid").Set("deleted_at = now()").Where("uuid = ? AND deleted_at IS NULL", UUID).Exec(ctx, &accountResult.UUID)
	if err != nil {
		slog.Error(types.AccountRepository, "SoftDelete", err)
		return nil, err
	}
	return &accountResult, nil
}

func AddGroup(ctx context.Context, UUID string, idGroup uint) (*models.Account, error) {
	var accountResult models.Account
	accountGroupModel := models.AccountGroup{UUID: UUID, IdGroup: uint64(idGroup)}
	_, err := db.Bun.NewInsert().Column("uuid", "id_group").Model(&accountGroupModel).Returning("uuid").Exec(ctx, &accountResult.UUID)
	if err != nil {
		slog.Error(types.AccountRepository, "AddGroup", err)
		return nil, err
	}
	return &accountResult, nil
}

func RemoveGroup(ctx context.Context, UUID string, idGroup uint) (*models.Account, error) {
	var accountResult models.Account
	accountGroupModel := models.AccountGroup{UUID: UUID, IdGroup: uint64(idGroup)}
	_, err := db.Bun.NewDelete().Model(&accountGroupModel).Where("id_group = ?", idGroup).Returning("uuid").Exec(ctx, &accountResult.UUID)
	if err != nil {
		slog.Error(types.AccountRepository, "RemoveGroup", err)
		return nil, err
	}
	return &accountResult, nil
}
