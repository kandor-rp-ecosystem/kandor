package models

import (
	"time"

	"github.com/uptrace/bun"
)

type Account struct {
	bun.BaseModel `bun:"table:account,alias:a"`
	UUID          string         `bun:"uuid,pk" json:"uuid"`
	Password      string         `bun:"password,notnull" json:"password"`
	Balance       float64        `bun:"balance,default:0" json:"balance"`
	MinedBlocks   uint64         `bun:"mined_blocks,default:0" json:"mined_blocks"`
	Email         string         `bun:"email" json:"email"`
	EmailVerified bool           `bun:"email_verified" json:"email_verified"`
	LastLogin     string         `bun:"last_login" json:"last_login"`
	CreatedAt     time.Time      `bun:"created_at,default:current_timestamp" json:"created_at"`
	UpdatedAt     time.Time      `bun:"updated_at,default:current_timestamp" json:"updated_at"`
	DeletedAt     time.Time      `bun:"deleted_at,default:null" json:"deleted_at"`
	AccountGroups []AccountGroup `bun:"rel:has-many,join:uuid=uuid" json:"groups"`
}

type AccountGroup struct {
	bun.BaseModel `bun:"table:account_group,alias:ag"`
	UUID          string    `bun:"uuid,pk," json:"uuid"`
	IdGroup       uint64    `bun:"id_group,pk" json:"id_group"`
	CreatedAt     time.Time `bun:"created_at,default:current_timestamp" json:"created_at"`
}

type Group struct {
	bun.BaseModel    `bun:"table:group,alias:g"`
	Id               uint              `bun:"id,pk,autoincrement" json:"id"`
	Name             string            `bun:"name" json:"name"`
	Description      string            `bun:"description" json:"description"`
	Prefix           string            `bun:"prefix" json:"prefix"`
	Suffix           string            `bun:"suffix" json:"suffix"`
	Accounts         []AccountGroup    `bun:"rel:has-many,join:id=id_group" json:"accounts,omitempty"`
	GroupPermissions []GroupPermission `bun:"rel:has-many,join:id=id_group" json:"groups,omitempty"`
}

type GroupPermission struct {
	bun.BaseModel `bun:"table:group_permission,alias:gp"`
	IdGroup       uint      `bun:"id_group,pk" json:"id_group"`
	IdPermission  uint      `bun:"id_permission,pk" json:"id_permission"`
	CreatedAt     time.Time `bun:"created_at,default:current_timestamp" json:"created_at"`
	IsNegated     bool      `bun:"is_negated,default:false" json:"is_negated"`
	World         string    `bun:"world,default:*" json:"world"`
	ExpiresAt     time.Time `bun:"expires_at,default:null" json:"expires_at"`
}

type Permission struct {
	bun.BaseModel `bun:"table:permission,alias:p"`
	Id            uint64 `bun:"id,pk,autoincrement" json:"id"`
	Name          string `bun:"name" json:"name"`
	Description   string `bun:"description" json:"description"`
}
