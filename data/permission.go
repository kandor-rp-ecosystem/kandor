package data

import (
	"context"
	"log/slog"

	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/db"
	"gitlab.com/kandor-rp-ecosystem/kandor/types"
)

func GetPermissionById(ctx context.Context, idPermission uint) (*models.Permission, error) {
	var permissionResult models.Permission
	err := db.Bun.NewSelect().Model(&permissionResult).Where("id = ?", idPermission).Scan(ctx)
	if err != nil {
		slog.Error(types.PermissionRepository, "GetPermissionById", err)
		return nil, err
	}
	return &permissionResult, nil
}

func GetPermissionByName(ctx context.Context, name string) (*models.Permission, error) {
	var permissionResult models.Permission
	err := db.Bun.NewSelect().Model(&permissionResult).Where("name = ?", name).Scan(ctx)
	if err != nil {
		slog.Error(types.PermissionRepository, "GetPermissionByName", err)
		return nil, err
	}
	return &permissionResult, nil
}

// TODO: esto está mal, hay que devolver metadata de la paginación
func GetAllPermissionsWithPagination(ctx context.Context, page int, size int) ([]models.Permission, error) {
	var permissions []models.Permission
	err := db.Bun.NewSelect().Model(&permissions).Limit(size).Offset((page - 1) * size).Scan(ctx)
	if err != nil {
		slog.Error(types.PermissionRepository, "GetAllPermissionsWithPagination", err)
		return nil, err
	}
	return permissions, nil
}

func CreatePermission(ctx context.Context, permission models.Permission) (*models.Permission, error) {
	var permissionResult models.Permission
	_, err := db.Bun.NewInsert().Model(&permission).Returning("id").Exec(ctx, &permissionResult.Id)
	if err != nil {
		slog.Error(types.PermissionRepository, "CreateGroup", err)
		return nil, err
	}
	return &models.Permission{Id: permissionResult.Id}, nil
}

func UpdatePermission(ctx context.Context, id uint, permission models.Permission) (*models.Permission, error) {
	var permissionResult models.Permission
	_, err := db.Bun.NewUpdate().Model(&permission).OmitZero().Returning("id").Where("id = ?", id).Exec(ctx, &permissionResult.Id)
	if err != nil {
		slog.Error(types.GroupRepository, "UpdateGroup", err)
		return nil, err
	}
	return &permissionResult, nil
}
