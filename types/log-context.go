package types

const (
	AccountRepository    = "AccountRepository"
	GroupRepository      = "GroupRepository"
	PermissionRepository = "PermissionRepository"
)
