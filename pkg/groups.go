package pkg

import (
	"context"

	"gitlab.com/kandor-rp-ecosystem/kandor/data"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
)

func GetGroupById(ctx context.Context, idGroup uint) (*models.Group, error) {
	group, err := data.GetGroupById(ctx, idGroup)
	if err != nil {
		return nil, err
	}
	return group, nil
}

func GetGroupByName(ctx context.Context, name string) (*models.Group, error) {
	group, err := data.GetGroupByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return group, nil
}

// TODO: esto está mal, hay que devolver metadata de la paginación
func GetAllGroupsWithPagination(ctx context.Context, page int, size int) ([]models.Group, error) {
	groups, err := data.GetAllGroupsWithPagination(ctx, page, size)
	if err != nil {
		return nil, err
	}
	return groups, nil
}

func CreateGroup(ctx context.Context, groupDto types.Group) (*types.GroupId, error) {
	group, err := data.CreateGroup(ctx, models.Group{
		Name:        groupDto.Name,
		Description: groupDto.Description,
		Prefix:      groupDto.Prefix,
		Suffix:      groupDto.Suffix,
	})
	if err != nil {
		return nil, err
	}
	return &types.GroupId{Id: group.Id}, nil
}

func UpdateGroup(ctx context.Context, id uint, updateDto types.Group) (*types.GroupId, error) {
	group, err := data.UpdateGroup(ctx, id, models.Group{
		Name:        updateDto.Name,
		Description: updateDto.Description,
		Prefix:      updateDto.Prefix,
		Suffix:      updateDto.Suffix,
	})
	if err != nil {
		return nil, err
	}
	return &types.GroupId{Id: group.Id}, nil
}

func DeleteGroup(ctx context.Context, idGroup uint) (*types.GroupId, error) {
	groupResult, err := data.DeleteGroup(ctx, idGroup)
	if err != nil {
		return nil, err
	}
	return &types.GroupId{Id: groupResult.Id}, nil
}

func AddPermissionToGroup(ctx context.Context, idGroup uint, idPermission uint) (*models.Group, error) {
	var groupResult models.Group
	return &models.Group{Id: groupResult.Id}, nil
}

func RemovePermissionFromGroup(ctx context.Context, idGroup uint, idPermission uint) (*models.Group, error) {
	var groupResult models.Group
	return &models.Group{Id: groupResult.Id}, nil
}
