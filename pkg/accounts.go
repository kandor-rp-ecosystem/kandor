package pkg

import (
	"context"
	"time"

	"gitlab.com/kandor-rp-ecosystem/kandor/data"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
)

func GetAccount(ctx context.Context, UUID string) (*types.Account, error) {
	account, err := data.GetAccount(ctx, UUID)
	if err != nil {
		return nil, err
	}
	return &types.Account{
		UUID:          account.UUID,
		Password:      account.Password,
		Balance:       account.Balance,
		MinedBlocks:   account.MinedBlocks,
		Email:         account.Email,
		EmailVerified: account.EmailVerified,
		CreatedAt:     account.CreatedAt,
		UpdatedAt:     account.UpdatedAt,
		DeletedAt:     account.DeletedAt,
	}, nil
}

func GetAccounts(ctx context.Context) ([]types.Account, error) {
	accounts, err := data.GetAccounts(ctx)
	if err != nil {
		return nil, err
	}
	var accountsResult []types.Account
	for _, account := range accounts {
		accountsResult = append(accountsResult, types.Account{
			UUID:          account.UUID,
			Password:      account.Password,
			Balance:       account.Balance,
			MinedBlocks:   account.MinedBlocks,
			Email:         account.Email,
			EmailVerified: account.EmailVerified,
			CreatedAt:     account.CreatedAt,
			UpdatedAt:     account.UpdatedAt,
			DeletedAt:     account.DeletedAt,
		})
	}
	return accountsResult, nil
}

func UpdateAccount(ctx context.Context, UUID string, updateDto types.UpdateAccount) (*types.AccountUUID, error) {
	account, err := data.UpdateAccount(ctx, UUID, models.Account{
		Password:      updateDto.Password,
		Balance:       updateDto.Balance,
		MinedBlocks:   updateDto.MinedBlocks,
		Email:         updateDto.Email,
		EmailVerified: updateDto.EmailVerified,
		UpdatedAt:     time.Now(),
	})
	if err != nil {
		return nil, err
	}
	return &types.AccountUUID{UUID: account.UUID}, nil
}

func SoftDelete(ctx context.Context, UUID string) (*types.AccountUUID, error) {
	account, err := data.SoftDelete(ctx, UUID)
	if err != nil {
		return nil, err
	}
	return &types.AccountUUID{UUID: account.UUID}, nil
}

func AddGroup(ctx context.Context, UUID string, idGroup uint) (*types.AccountUUID, error) {
	account, err := data.AddGroup(ctx, UUID, idGroup)
	if err != nil {
		return nil, err
	}
	return &types.AccountUUID{UUID: account.UUID}, nil
}

func RemoveGroup(ctx context.Context, UUID string, idGroup uint) (*types.AccountUUID, error) {
	account, err := data.RemoveGroup(ctx, UUID, idGroup)
	if err != nil {
		return nil, err
	}
	return &types.AccountUUID{UUID: account.UUID}, nil
}
