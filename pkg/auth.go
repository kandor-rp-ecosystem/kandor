package pkg

import (
	"context"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/kandor-rp-ecosystem/kandor/data"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
	utils "gitlab.com/kandor-rp-ecosystem/kandor/util"
)

func RegisterAccount(ctx context.Context, account types.RegisterAccount) (accountResult *types.AccountUUID, err error) {
	var accountModel *models.Account
	hashedPassword, err := utils.HashPassword(account.Password)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	a := models.Account{
		UUID:     account.UUID,
		Password: hashedPassword,
	}
	accountModel, err = data.RegisterAccount(ctx, a)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	accountResult = &types.AccountUUID{
		UUID: accountModel.UUID,
	}
	return accountResult, nil
}

func LoginAccount(ctx context.Context, loginDto types.LoginAccountWithMinecraft) (*types.LoginResponse, error) {
	account, err := data.LoginAccountWithMinecraft(ctx, models.Account{
		UUID:     loginDto.UUID,
		Password: loginDto.Password,
	})
	if err != nil {
		return nil, err
	}
	samePassword := utils.CheckPasswordHash(loginDto.Password, account.Password)
	if !samePassword {
		return nil, err
	}
	data.UpdateLastLogin(ctx, loginDto.UUID)
	token, err := generateToken(ctx, loginDto.UUID)
	return &types.LoginResponse{Token: *token}, nil
}

func generateToken(ctx context.Context, UUID string) (*string, error) {
	mySigningKey := []byte("SECRET") //TODO: Change this to a env variable
	expirationTime := time.Now().Add(5 * time.Minute)
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["UUID"] = UUID
	claims["exp"] = expirationTime.Unix()
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return nil, err
	}
	return &tokenString, nil
}
