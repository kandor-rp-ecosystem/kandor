package pkg

import (
	"context"

	"gitlab.com/kandor-rp-ecosystem/kandor/data"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/models"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
)

func GetPermissionById(ctx context.Context, idPermission uint) (*types.Permission, error) {
	permission, err := data.GetPermissionById(ctx, idPermission)
	if err != nil {
		return nil, err
	}
	return &types.Permission{Id: permission.Id, Name: permission.Name, Description: permission.Description}, nil
}

func GetPermissionByName(ctx context.Context, name string) (*types.Permission, error) {
	permission, err := data.GetPermissionByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return &types.Permission{Id: permission.Id, Name: permission.Name, Description: permission.Description}, nil
}

func GetAllPermissionsWithPagination(ctx context.Context, page int, size int) ([]types.Permission, error) {
	permissions, err := data.GetAllPermissionsWithPagination(ctx, page, size)
	if err != nil {
		return nil, err
	}
	var permissionsDto []types.Permission
	for _, permission := range permissions {
		permissionDto := types.Permission{
			Id:          permission.Id,
			Name:        permission.Name,
			Description: permission.Description,
		}
		permissionsDto = append(permissionsDto, permissionDto)
	}
	return permissionsDto, nil
}

func CreatePermission(ctx context.Context, groupDto types.CreatePermission) (*types.PermissionId, error) {
	group, err := data.CreatePermission(ctx, models.Permission{
		Name:        groupDto.Name,
		Description: groupDto.Description,
	})
	if err != nil {
		return nil, err
	}
	return &types.PermissionId{Id: group.Id}, nil
}

func UpdatePermission(ctx context.Context, id uint, updateDto types.UpdatePermission) (*types.PermissionId, error) {
	permission, err := data.UpdatePermission(ctx, id, models.Permission{
		Name:        updateDto.Name,
		Description: updateDto.Description,
	})
	if err != nil {
		return nil, err
	}
	return &types.PermissionId{Id: permission.Id}, nil
}
