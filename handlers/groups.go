package handlers

import (
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
	"gitlab.com/kandor-rp-ecosystem/kandor/pkg"
)

func GetGroupById(c echo.Context) error {
	var idGroup uint64
	groupId := c.Param("idGroup")
	// Parse the idGroup to uint64
	idGroup, err := strconv.ParseUint(groupId, 10, 64)
	if err != nil {
		return c.JSON(400, err)
	}
	groupDto, err := pkg.GetGroupById(c.Request().Context(), uint(idGroup))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, groupDto)
}

func GetGroupByName(c echo.Context) error {
	groupDto, err := pkg.GetGroupByName(c.Request().Context(), c.Param("name"))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, groupDto)
}

func GetAllGroupsWithPagination(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return c.JSON(400, err)
	}
	size, err := strconv.Atoi(c.QueryParam("size"))
	if err != nil {
		return c.JSON(400, err)
	}
	groupsDto, err := pkg.GetAllGroupsWithPagination(c.Request().Context(), page, size)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, groupsDto)
}

func CreateGroup(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.CreateGroup{}
	if err := binder.Bind(&request, c); err != nil {
		return c.JSON(400, err)
	}
	err := request.Validate()
	if err != nil {
		return c.JSON(400, err)
	}
	group := types.Group{
		Name:        request.Name,
		Description: request.Description,
		Prefix:      request.Prefix,
		Suffix:      request.Suffix,
	}
	groupDto, err := pkg.CreateGroup(c.Request().Context(), group)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(201, groupDto)
}

// TODO: repasar lo de err
func UpdateGroup(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.UpdateGroup{}
	if err := binder.Bind(&request, c); err != nil {
		return c.JSON(400, err)
	}
	id := c.Param("idGroup")
	idGroup, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return c.JSON(400, err)
	}
	err = request.Validate()
	if err != nil {
		return c.JSON(400, err)
	}
	group := types.Group{
		Name:        request.Name,
		Description: request.Description,
		Prefix:      request.Prefix,
		Suffix:      request.Suffix,
	}
	groupDto, err := pkg.UpdateGroup(c.Request().Context(), uint(idGroup), group)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, groupDto)
}

func DeleteGroup(c echo.Context) error {
	var idGroup uint64
	groupId := c.Param("idGroup")
	// Parse the idGroup to uint64
	idGroup, err := strconv.ParseUint(groupId, 10, 64)
	if err != nil {
		return c.JSON(400, err)
	}
	group, err := pkg.DeleteGroup(c.Request().Context(), uint(idGroup))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, group)
}
