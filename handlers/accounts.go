package handlers

import (
	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
	"gitlab.com/kandor-rp-ecosystem/kandor/pkg"
	utils "gitlab.com/kandor-rp-ecosystem/kandor/util"
)

func GetAccount(c echo.Context) error {
	accountDto, err := pkg.GetAccount(c.Request().Context(), c.Param("uuid"))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, accountDto)
}

func GetAccounts(c echo.Context) error {
	accountsDto, err := pkg.GetAccounts(c.Request().Context())
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, accountsDto)
}

func UpdateAccount(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.UpdateAccount{}
	if err := binder.Bind(&request, c); err != nil {
		return c.JSON(400, err)
	}
	err := request.Validate()
	if err != nil {
		return c.JSON(400, err)
	}
	updateResponseDto, err := pkg.UpdateAccount(c.Request().Context(), c.Param("uuid"), request)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, updateResponseDto)
}

func SoftDelete(c echo.Context) error {
	softDeleteResponseDto, err := pkg.SoftDelete(c.Request().Context(), c.Param("uuid"))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, softDeleteResponseDto)
}

func AddGroup(c echo.Context) error {
	val, err := utils.ParseUint(c.Param("idGroup"))
	if err != nil {
		return c.JSON(400, err)
	}
	addGroupResponseDto, err := pkg.AddGroup(c.Request().Context(), c.Param("uuid"), *val)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, addGroupResponseDto)
}

func RemoveGroup(c echo.Context) error {
	val, err := utils.ParseUint(c.Param("idGroup"))
	if err != nil {
		return c.JSON(400, err)
	}
	addGroupResponseDto, err := pkg.RemoveGroup(c.Request().Context(), c.Param("uuid"), *val)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, addGroupResponseDto)
}
