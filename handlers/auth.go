package handlers

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
	"gitlab.com/kandor-rp-ecosystem/kandor/pkg"
)

func RegisterAccount(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.RegisterAccount{}
	if err := binder.Bind(&request, c); err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	err := request.Validate()
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	registerResponseDto, err := pkg.RegisterAccount(c.Request().Context(), request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	return c.JSON(201, registerResponseDto)
}

func LoginAccount(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.LoginAccountWithMinecraft{}
	if err := binder.Bind(&request, c); err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	err := request.Validate()
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	loginResponseDto, err := pkg.LoginAccount(c.Request().Context(), request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	return c.JSON(200, loginResponseDto)
}
