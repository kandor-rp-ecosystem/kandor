package handlers

import (
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/kandor-rp-ecosystem/kandor/data/types"
	"gitlab.com/kandor-rp-ecosystem/kandor/pkg"
)

func GetPermissionById(c echo.Context) error {
	var idPermission uint64
	permissionId := c.Param("idPermission")
	idPermission, err := strconv.ParseUint(permissionId, 10, 64)
	if err != nil {
		return c.JSON(400, err)
	}
	permissionDto, err := pkg.GetPermissionById(c.Request().Context(), uint(idPermission))
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, permissionDto)
}

func GetPermissionByName(c echo.Context) error {
	permissionName := c.Param("idPermission")
	permissionDto, err := pkg.GetPermissionByName(c.Request().Context(), permissionName)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, permissionDto)
}

func GetAllPermissionsWithPagination(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return c.JSON(400, err)
	}
	size, err := strconv.Atoi(c.QueryParam("size"))
	if err != nil {
		return c.JSON(400, err)
	}
	groupsDto, err := pkg.GetAllPermissionsWithPagination(c.Request().Context(), page, size)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, groupsDto)
}

func CreatePermission(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.CreatePermission{}
	if err := binder.Bind(&request, c); err != nil {
		return c.JSON(400, err)
	}
	err := request.Validate()
	if err != nil {
		return c.JSON(400, err)
	}
	permission := types.CreatePermission{
		Name:        request.Name,
		Description: request.Description,
	}
	permissionDto, err := pkg.CreatePermission(c.Request().Context(), permission)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(201, permissionDto)
}

// TODO: repasar lo de err
func UpdatePermission(c echo.Context) error {
	binder := echo.DefaultBinder{}
	request := types.UpdatePermission{}
	if err := binder.Bind(&request, c); err != nil {
		return c.JSON(400, err)
	}
	id := c.Param("idPermission")
	idGroup, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return c.JSON(400, err)
	}
	err = request.Validate()
	if err != nil {
		return c.JSON(400, err)
	}
	permission := types.UpdatePermission{
		Name:        request.Name,
		Description: request.Description,
	}
	permissionDto, err := pkg.UpdatePermission(c.Request().Context(), uint(idGroup), permission)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, permissionDto)
}
